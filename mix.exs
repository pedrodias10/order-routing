defmodule OrderRouting.MixProject do
  use Mix.Project

  def project do
    [
      app: :order_routing,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {OrderRouting.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.9.0"},
      {:plug, "~> 1.12"},
      {:plug_cowboy, "~> 2.5.1"},
      {:poison, "~> 4.0.1"},
      {:oban, "~> 2.8.0"},
      {:httpoison, "~> 1.8"},
      {:jason, "~> 1.2"},
      {:shorter_maps, "~> 2.0"},
      {:ecto, "~> 3.6.2"}
    ]
  end
end
