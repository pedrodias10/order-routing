use Mix.Config

config :order_routing,
  http_adapter: OrderRouting.HttpMock

config :order_routing, Oban, queues: false, plugins: false

config :order_routing, OrderRouting.Repo,
  database: "exercise_test",
  hostname: "localhost",
  port: 5433,
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 20

config :order_routing, allowed_operations: [:create, :update]

config :order_routing,
  create: [
    OrderRouting.Listeners.ListnerOne
  ]

config :order_routing,
  update: [
    OrderRouting.Listeners.ListnerOne,
    OrderRouting.Listeners.ListnerTwo
  ]
