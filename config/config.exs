use Mix.Config

config :order_routing,
  http_adapter: HTTPoison

config :order_routing, OrderRouting.Endpoint, port: 4001

config :order_routing, OrderRouting.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "exercise",
  port: 5433,
  pool_size: 10

config :order_routing,
  ecto_repos: [OrderRouting.Repo]

config :order_routing, Oban,
  plugins: [],
  repo: OrderRouting.Repo,
  queues: [listener: 50, listeners: 10]

config :order_routing, allowed_operations: [:create, :update]

config :order_routing,
  create: [
    OrderRouting.Listeners.ListnerOne
  ]

config :order_routing,
  update: [
    OrderRouting.Listeners.ListnerOne,
    OrderRouting.Listeners.ListnerTwo
  ]

config :plug, :validate_header_keys_during_test, false

import_config "#{Mix.env()}.exs"
