# OrderRouting

Receives API calls and propagates to all listeners

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `order_routing` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:order_routing, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/order_routing](https://hexdocs.pm/order_routing).

