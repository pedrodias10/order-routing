defmodule OrderRouting.EndpointTest do
  use ExUnit.Case, async: true
  use Plug.Test

  import ExUnit.CaptureLog

  alias OrderRouting.Endpoint

  test "handles post new order" do
    assert capture_log(fn ->
             response =
               conn(:post, "/order/new", %{shinny_property: "awesome"})
               |> Endpoint.call([])

             assert response.status == 200
           end) =~
             "[info]  OrderRouting received :create event with body: %{\"shinny_property\" => \"awesome\"}"
  end

  test "handles post update order" do
    assert capture_log(fn ->
             response =
               conn(:post, "/order/update", %{shinny_property: "awesome"})
               |> Endpoint.call([])

             assert response.status == 200
           end) =~
             "[info]  OrderRouting received :update event with body: %{\"shinny_property\" => \"awesome\"}"
  end

  test "handles non known operation order" do
    assert_raise Plug.Conn.WrapperError, fn ->
      conn(:post, "/order/delete", %{shinny_property: "awesome"})
      |> Endpoint.call([])
    end
  end
end
