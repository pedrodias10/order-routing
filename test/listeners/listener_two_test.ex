defmodule OrderRouting.Listeners.ListnerTwoTest do
  use ExUnit.Case, async: true
  use Plug.Test

  test "handles notify new orders" do
    body = %{test: "body"}

    assert {:ok, %{"response" => "ok"}} =
             OrderRouting.Listeners.ListnerTwo.forward("create", body)
  end
end
