defmodule OrderRouting.SpawnListenersTest do
  use ExUnit.Case, async: true
  use Plug.Test

  use Oban.Testing, repo: OrderRouting.Repo

  alias OrderRouting.Workers.{SpawnListeners, SpawnListener}

  test "handles notify new orders" do
    body = %{test: "body"}
    assert :ok = perform_job(SpawnListeners, %{event: :create, body: body})

    assert_enqueued(
      worker: SpawnListener,
      args: %{event: :create, listener: OrderRouting.Listeners.ListnerOne, body: body}
    )
  end

  test "handles notify update orders" do
    body = %{test: "body"}
    assert :ok = perform_job(SpawnListeners, %{event: :update, body: body})

    # we assume the first one is the last inserted
    assert [
             %{
               args: %{
                 "event" => "update",
                 "listener" => "Elixir.OrderRouting.Listeners.ListnerTwo",
                 "body" => %{"test" => "body"}
               }
             },
             %{
               args: %{
                 "event" => "update",
                 "listener" => "Elixir.OrderRouting.Listeners.ListnerOne",
                 "body" => %{"test" => "body"}
               }
             }
           ] = all_enqueued(worker: SpawnListener)
  end

  test "handles non known notify order" do
    body = %{test: "body"}
    assert :ok = perform_job(SpawnListeners, %{event: :delete, body: body})

    assert [] = all_enqueued(worker: SpawnListener)
  end
end
