defmodule OrderRouting.SpawnListenerTest do
  use ExUnit.Case, async: true
  use Plug.Test

  use Oban.Testing, repo: OrderRouting.Repo

  alias OrderRouting.Workers.SpawnListener

  test "handles listener call" do
    body = %{id: 1, test: "body"}

    assert :ok =
             perform_job(SpawnListener, %{
               event: :update,
               listener: OrderRouting.Listeners.ListnerTwo,
               body: body
             })
  end

  test "fails listener call" do
    body = %{id: 1, test: "body"}

    assert_raise UndefinedFunctionError, fn ->
      perform_job(SpawnListener, %{
        event: :update,
        listener: "Elixir.OrderRouting.Listeners.ListnerThree",
        body: body
      })
    end
  end
end
