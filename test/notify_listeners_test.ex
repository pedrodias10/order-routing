defmodule OrderRouting.EndpointTest do
  use ExUnit.Case, async: true
  use Plug.Test

  use Oban.Testing, repo: OrderRouting.Repo

  import ExUnit.CaptureLog

  alias OrderRouting.NotifyListeners
  alias OrderRouting.Workers.SpawnListeners

  test "handles notify new order" do
    body = %{property: "x"}

    assert capture_log(fn ->
             assert NotifyListeners.notify(:create, body) == :ok
             assert_enqueued(worker: SpawnListeners, args: %{event: :create, body: body})
           end) =~
             "[info]  OrderRouting received :create event with body: %{property: \"x\"}"
  end

  test "handles notify update order" do
    body = %{property: "x"}

    assert capture_log(fn ->
             assert NotifyListeners.notify(:update, body) == :ok
             assert_enqueued(worker: SpawnListeners, args: %{event: :update, body: body})
           end) =~
             "[info]  OrderRouting received :update event with body: %{property: \"x\"}"
  end

  test "handles non known notify order" do
    body = %{property: "x"}

    assert capture_log(fn ->
             assert NotifyListeners.notify(:delete, body) == :ok
             refute_enqueued(worker: SpawnListeners, args: %{event: :delete, body: body})
           end) =~
             "[error] OrderRouting is not able to handle :delete event, body: %{property: \"x\"}"
  end
end
