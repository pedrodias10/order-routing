defmodule OrderRouting.Repo do
  use Ecto.Repo,
    otp_app: :order_routing,
    adapter: Ecto.Adapters.Postgres
end
