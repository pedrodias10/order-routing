defmodule OrderRouting.HttpWrapper do
  @doc """
  Kindly fetched from here: https://blog.lelonek.me/how-to-mock-httpoison-in-elixir-7947917a9266
  """
  @http_adapter Application.get_env(:order_routing, :http_adapter)

  def post(url, body, options) do
    url
    |> @http_adapter.post(body, [], options)
    |> process_response_body(url)
  end

  defp process_response_body(
         {:ok, %HTTPoison.Response{status_code: status_code, body: body}},
         _url
       )
       when status_code in [200, 201, 204],
       do: {:ok, parse_body(body)}

  defp process_response_body(
         {:ok, %HTTPoison.Response{status_code: status_code, body: body, headers: _headers}},
         url
       ) do
    {
      :error,
      %{
        status_code: status_code,
        body: parse_body(body),
        url: url
      }
    }
  end

  defp process_response_body({:error, %HTTPoison.Error{reason: {_, reason}}}, _url),
    do: {:error, reason}

  defp process_response_body({:error, %HTTPoison.Error{reason: reason}}, _url),
    do: {:error, reason}

  defp process_response_body({:error, reason}, _url),
    do: {:error, reason}

  defp parse_body(body) do
    with {:ok, body} <- Jason.decode(body) do
      body
    else
      _ -> body
    end
  end
end
