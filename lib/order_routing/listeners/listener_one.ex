defmodule OrderRouting.Listeners.ListnerOne do
  @behaviour OrderRouting.Listener

  require Logger

  @impl OrderRouting.Listener
  def forward(type, body) do
    Logger.info("Starting listener one order operation broadcast")
    # in the real world here we would sign the request and get this url from a config,
    # and maybe digest some of the body
    url = "https://thelistenerone.url/#{type}"
    OrderRouting.HttpWrapper.post(url, body, [])
  end
end
