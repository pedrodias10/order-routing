defmodule OrderRouting.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [OrderRouting.Repo, OrderRouting.Endpoint, {Oban, oban_config()}]

    opts = [strategy: :one_for_one, name: OrderRouting.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp oban_config do
    Application.fetch_env!(:order_routing, Oban)
  end
end
