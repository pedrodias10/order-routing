defmodule OrderRouting.Listener do
  @doc """
  Forwards a event.
  """
  @callback forward(tuple(), map()) :: {:ok, term} | {:error, String.t()}
end
