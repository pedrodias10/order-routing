defmodule OrderRouting.HttpMock do
  def post(_url, _body, _headers, _options) do
    {:ok, %HTTPoison.Response{status_code: 200, body: ~s|{"response": "ok"}|}}
  end
end
