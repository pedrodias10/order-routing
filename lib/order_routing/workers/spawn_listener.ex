defmodule OrderRouting.Workers.SpawnListener do
  @doc """
  Worker that spawns a single listener
  """

  use Oban.Worker,
    max_attempts: 10,
    queue: :listener

  import ShorterMaps

  require Logger

  @impl Oban.Worker
  def perform(%Oban.Job{args: ~m{listener, event, body}}) do
    apply(String.to_atom(listener), :forward, [event, body])
    |> handle_completion(body["id"])
  end

  defp handle_completion({:ok, msg}, id) do
    Logger.info("Order #{id} logged with success with the response #{inspect(msg)}")
    :ok
  end

  defp handle_completion({:error, msg} = error, id) do
    Logger.info("Order #{id} logged with error #{inspect(msg)}")
    error
  end
end
