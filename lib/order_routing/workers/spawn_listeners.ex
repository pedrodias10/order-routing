defmodule OrderRouting.Workers.SpawnListeners do
  @doc """
  Worker that spawns all the listeners for the given operation
  """

  use Oban.Worker,
    max_attempts: 10,
    queue: :listeners

  require Logger

  import ShorterMaps

  @impl Oban.Worker
  def perform(%Oban.Job{args: ~m{event, body}}) do
    for listener <- Application.get_env(:order_routing, String.to_atom(event), []) do
      ~M{event, listener, body}
      |> OrderRouting.Workers.SpawnListener.new()
      |> Oban.insert()
    end

    :ok
  end
end
