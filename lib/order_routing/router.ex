defmodule OrderRouting.Router do
  use Plug.Router

  alias OrderRouting.NotifyListeners

  plug(:match)
  plug(:dispatch)

  post "/new" do
    resp = NotifyListeners.notify(:create, conn.body_params)

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(resp))
  end

  post "/update" do
    resp = NotifyListeners.notify(:update, conn.body_params)

    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(resp))
  end
end
