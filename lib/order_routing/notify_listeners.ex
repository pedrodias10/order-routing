defmodule OrderRouting.NotifyListeners do
  require Logger

  import ShorterMaps

  @operations Application.get_env(:order_routing, :allowed_operations, [])

  @spec notify(tuple(), Map.t()) :: :ok
  def notify(event, body)

  def notify(event, body) when event in @operations do
    ~M{event, body}
    |> OrderRouting.Workers.SpawnListeners.new()
    |> Oban.insert()

    Logger.info("OrderRouting received #{inspect(event)} event with body: #{inspect(body)}")

    :ok
  end

  def notify(event, body),
    do:
      Logger.error(
        "OrderRouting is not able to handle #{inspect(event)} event, body: #{inspect(body)} given"
      )
end
